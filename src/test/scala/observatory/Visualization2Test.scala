package observatory

import org.scalatest.FunSuite
import org.scalacheck.Gen
import org.scalatest.prop.Checkers
import org.scalacheck.Prop._

trait Visualization2Test extends FunSuite with Checkers {

  test("bilinearInterpolation#1") {
    val pointGen = for (
      lat <- Gen.choose(0.0, 1.0);
      lon <- Gen.choose(0.0, 1.0);
      temp1 <- Gen.choose(-60.0, 60.0);
      temp2 <- Gen.choose(-60.0, 60.0);
      temp3 <- Gen.choose(-60.0, 60.0);
      temp4 <- Gen.choose(-60.0, 60.0)
    ) yield (CellPoint(lat, lon), temp1, temp2, temp3, temp4)
    check(forAll(pointGen) { x =>
      val interpolTemp = Visualization2.bilinearInterpolation(x._1, x._2, x._3, x._4, x._5)
      interpolTemp >= Seq(x._2, x._3, x._4, x._5).min && interpolTemp <= Seq(x._2, x._3, x._4, x._5).max
    })

  }

  test("bilinearInterpolation#2") {
    assert(Visualization2.bilinearInterpolation(CellPoint(0.5, 0.5), 10, 20, 30, 40) === 25.0)
    assert(Visualization2.bilinearInterpolation(CellPoint(0.1, 0.5), 10, 20, 30, 40) === 17.0)
    assert(Visualization2.bilinearInterpolation(CellPoint(0.5, 0.1), 10, 20, 30, 40) === 21.0)
    assert(Visualization2.bilinearInterpolation(CellPoint(0.9, 0.1), 10, 20, 30, 40) === 29.0)
    assert(Visualization2.bilinearInterpolation(CellPoint(1.0, 0.0), 10, 20, 30, 40) === 30.0)
  }

  test("visualizeGrid") {
    val temperatures = Seq(
      (Location(90.0, 0), -60.0),
      (Location(0.0, 90.0), 60.0),
      (Location(0.0, 0.0), 60.0),
      (Location(0.0, -90.0), 60.0),
      (Location(-89.0, 0), -60.0))
    val gridTempFunc = Manipulation.makeGrid(temperatures)
    val img = Visualization2.visualizeGrid(gridTempFunc, Visualization.temperatureColors, Tile(0, 0, 0))
    assert(Color(img.pixel(128, 128).red, img.pixel(128, 128).green, img.pixel(128, 128).blue) == Color(255, 255, 255))
  }

//  test("genTilesFromGrid (year 2015, zoom 3)") {
//    def genImage(year: Int, tile: Tile, gridMap: GridLocation => Temperature): Unit = {
//      val img = Visualization2.visualizeGrid(gridMap, Visualization.temperatureColors, tile, 128).scaleTo(256, 256)
//      new java.io.File(s"target/temperatures/$year/${tile.zoom}").mkdirs()
//      img.output(new java.io.File(s"target/temperatures/$year/${tile.zoom}/${tile.x}-${tile.y}.png"))
//      ()
//    }
//    val avgTemps = Seq((2015, Manipulation.makeGrid(Extraction.locationYearlyAverageRecords(Extraction.locateTemperatures(2015, "/stations.csv", "/2015.csv")))))
//    println("average temps grid calculated")
//    Interaction.generateTiles[GridLocation => Temperature](
//      avgTemps,
//      genImage _,
//      (3 to 3))
//    assert(true)
//  }

}
