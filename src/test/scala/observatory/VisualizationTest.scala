package observatory

import org.scalatest.FunSuite
import org.scalatest.prop.Checkers

trait VisualizationTest extends FunSuite with Checkers {

  test("interpolateValue") {
    assert(Visualization.interpolateValue((3.1, 3),(2.0, 2), 2.5) == 2)
  }

  test("interpolateColor#1") {
    val tempCol1 = (-60.0, Color(0, 0, 0))
    val tempCol2 = (-50.0, Color(33, 0, 107))
    assert(Visualization.interpolateColor(tempCol1, tempCol2, -55.0) == Color(17, 0, 54))
  }

  test("interpolateColor#2") {
    val tempCol1 = (-2.147483648E9, Color(255, 0, 0))
    val tempCol2 = (1.0, Color(0, 0, 255))
    assert(Visualization.interpolateColor(tempCol1, tempCol2, -1.0737418235E9) == Color(128, 0, 128))
  }

  test("interpolateColor#3") {
    val tempColList = List((-2.147483648E9, Color(255, 0, 0)), (1.0, Color(0, 0, 255)))
    assert(Visualization.interpolateColor(tempColList, -1.0737418235E9) == Color(128, 0, 128))
  }

  test("greatCircleDistance") {
    val newYork = Location(40.785091, -73.968285)
    val stuttgart = Location(48.777106, 9.180769)
    val distance = newYork.greatCircleDistance(stuttgart)
//    println(Visualization.centralAngle(newYork, stuttgart))
//    println(Visualization.meanEarthRadius)
    assert(math.abs(distance - 6295.0) < 10.0)
  }

  test("predictTemperature") {
    val loc1 = (Location(90, -180), 1.0)
    val loc2 = (Location(0, 0), 40.3)
    assert(Visualization.predictTemperature(List(loc1, loc2), Location(1, 20)) > 38.0)
  }

//  test("visualisation PNG") {
//    val img = Visualization.visualize(Extraction.locationYearlyAverageRecords(Extraction.locateTemperatures(2015, "/stations.csv", "/2015.csv")), Visualization.temperatureColors)
//    img.output(new java.io.File(s"src/test/resources/testEarth.png"))
//    println(img.pixel(37, 78).red + " " + img.pixel(37, 78).green + " " + img.pixel(37, 78).blue)
//    assert(true)
//  }

}
