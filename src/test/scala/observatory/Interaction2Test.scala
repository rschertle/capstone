package observatory

import org.scalatest.FunSuite
import org.scalatest.prop.Checkers
import org.scalacheck.Gen
import org.scalacheck.Prop._

trait Interaction2Test extends FunSuite with Checkers {

  test("year selection not out of bounds") {
    check {
      forAll(for (year <- Gen.choose(0, 9999)) yield year) { year =>
        Interaction2.yearBounds(Signal(Interaction2.availableLayers(0)))().contains(Interaction2.yearSelection(Signal(Interaction2.availableLayers(0)), Signal(year))())
      }

    }
  }

}
