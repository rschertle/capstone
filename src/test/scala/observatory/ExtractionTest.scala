package observatory

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner
import java.time.LocalDate
import org.scalatest.prop.Checkers

trait ExtractionTest extends FunSuite with Checkers {
  
  private val testData = Extraction.locateTemperatures(2015, "/stationsTest.csv", "/2015Test.csv").map{case (dat, loc, temp) => (dat,loc,math.round(temp * 10).toDouble / 10)}

  test("locateTemperatures") {
//    println(testData)
    assert(testData.toSet.equals(Set(
      (LocalDate.of(2015, 8, 11), Location(37.35, -78.433), 27.3),
      (LocalDate.of(2015, 12, 6), Location(37.358, -78.438), 0.0),
      (LocalDate.of(2015, 1, 29), Location(37.358, -78.438), 2.0))))
  }

  test("locationYearlyAverageRecords") {
    val funRes = Extraction.locationYearlyAverageRecords(testData)
//    println(funRes)
    assert(funRes.toSet.equals(Set(
      (Location(37.35, -78.433), 27.3),
      (Location(37.358, -78.438), 1.0))))
  }
  
}