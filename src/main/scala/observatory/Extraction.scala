package observatory

import java.time.LocalDate
import java.nio.file.Paths
import org.apache.spark.sql.types._
import org.apache.spark.sql.Row
import scala.io.Source
import org.apache.spark.sql.SparkSession

/**
 * 1st milestone: data extraction
 */
object Extraction {

  //  import org.apache.spark.sql.functions._

  // For implicit conversions like converting RDDs to DataFrames
  //  import spark.implicits._

  def fahrToCelcius(fahr: Double): Double = (fahr - 32) * 0.5555555555555556

  /**
   * @param year             Year number
   * @param stationsFile     Path of the stations resource file to use (e.g. "/stations.csv")
   * @param temperaturesFile Path of the temperatures resource file to use (e.g. "/1975.csv")
   * @return A sequence containing triplets (date, location, temperature)
   */
  def locateTemperatures(year: Year, stationsFile: String, temperaturesFile: String): Iterable[(LocalDate, Location, Temperature)] = {

    //Station file processing
    val stationMap = parseStationsFile(getClass.getResource(stationsFile).toURI.toString())

    //Temperature file processing
    locateTemperatures(year, stationMap, getClass.getResource(temperaturesFile).toURI.toString())

  }

  /**
   *  @param temperaturesFile URI of the temperatures resource file to use
   */
  def locateTemperatures(year: Year, stationsMap: Map[(String, String), (String, String)], temperaturesFile: String): Iterable[(LocalDate, Location, Temperature)] = {

    //Temperature file processing
    SparkSession.builder().getOrCreate().sparkContext.textFile(temperaturesFile).flatMap(line => {
      val words = line.split(',')
      stationsMap.get((words(0), words(1))) match {
        case Some(x) => Seq((LocalDate.of(year, words(2).toInt, words(3).toInt), Location(x._1.toDouble, x._2.toDouble), fahrToCelcius(words(4).toDouble)))
        case None    => List()
      }
    }).collect().toSeq
    //    Source.fromFile(Paths.get(getClass.getResource(temperaturesFile).toURI).toFile()).getLines().flatMap(line => {
    //      val words = line.split(',')
    //      stationsMap.get((words(0), words(1))) match {
    //        case Some(x) => Seq((LocalDate.of(year, words(2).toInt, words(3).toInt), Location(x._1.toDouble, x._2.toDouble), fahrToCelcius(words(4).toDouble)))
    //        case None    => List()
    //      }
    //    }).toSeq

  }

  def parseStationsFile(stationsFile: String): Map[(String, String), (String, String)] = {
    //Station file processing
    SparkSession.builder().getOrCreate().sparkContext.textFile(stationsFile).map(line => {
      val words = line.split(",", -1)
      ((words(0), words(1)), (words(2), words(3)))
    })
      .filter { case (station, pos) => !(pos._1.isEmpty || pos._2.isEmpty) }.collect().toMap
    //    Source.fromFile(Paths.get(getClass.getResource(stationsFile).toURI).toFile()).getLines().map(line => {
    //      val words = line.split(",", -1)
    //      ((words(0), words(1)), (words(2), words(3)))
    //    })
    //      .filter { case (station, pos) => !(pos._1.isEmpty || pos._2.isEmpty) }.toMap
  }

  //  def locateTemperaturesDf(year: Year, stationsFile: String, temperaturesFile: String): Iterable[(LocalDate, Location, Temperature)] = {
  //    //read station csv => Dataframe
  //    val stationSchema = new StructType(List(
  //      new StructField("STN", StringType, true),
  //      new StructField("WBAN", StringType, true),
  //      new StructField("Latitude", DoubleType, true),
  //      new StructField("Longitude", DoubleType, true)).toArray)
  //    val stationDF = spark.read.schema(stationSchema).csv(Paths.get(getClass.getResource(stationsFile).toURI).toString)
  //
  //    //read temperature csv => Dataframe
  //    val tempSchema = new StructType(List(
  //      new StructField("STN", StringType, true),
  //      new StructField("WBAN", StringType, true),
  //      new StructField("Month", IntegerType, true),
  //      new StructField("Day", IntegerType, true),
  //      new StructField("Temperature", DoubleType, true)).toArray)
  //    val tempDF = spark.read.schema(tempSchema).csv(Paths.get(getClass.getResource(temperaturesFile).toURI).toString).withColumn("Temperature", (col("Temperature") - 32) * 5 / 9)
  //
  //    //join
  //    val joinedDF = stationDF.filter($"Latitude".isNotNull).filter($"Longitude".isNotNull).join(tempDF, stationDF("STN") <=> tempDF("STN") && stationDF("WBAN") <=> tempDF("WBAN")).select("Month", "Day", "Latitude", "Longitude", "Temperature")
  //    //output conversion
  //    joinedDF.collect().map(r => (LocalDate.of(year, r.getAs[Int]("Month"), r.getAs[Int]("Day")), Location(r.getAs[Double]("Latitude"), r.getAs[Double]("Longitude")), r.getAs[Double]("Temperature")))
  //  }

  /**
   * @param records A sequence containing triplets (date, location, temperature)
   * @return A sequence containing, for each location, the average temperature over the year.
   */
  def locationYearlyAverageRecords(records: Iterable[(LocalDate, Location, Temperature)]): Iterable[(Location, Temperature)] = {

    records.groupBy(r => r._2).mapValues(temps => temps.map { _._3 }.sum / temps.size)
    //records.map { case (dat, loc, temp) => (loc, temp) }.groupBy(r => r._1).mapValues(temps => temps.foldLeft((0.toDouble))((a, b) => a + b._2) / temps.size)

  }

  /**
   *  @param temperaturesFile URI of the temperatures resource file to use
   */
  def locationYearlyAverageRecords(year: Year, stationsMap: Map[(String, String), (String, String)], temperaturesFile: String): Iterable[(Location, Temperature)] = {
    //Temperature file processing
    val res = SparkSession.builder().getOrCreate().sparkContext.textFile(temperaturesFile).flatMap[(Location, Temperature)](line => {
      val words = line.split(',')
      if (words.size >= 5) {
        stationsMap.get((words(0), words(1))) match {
          case Some(x) => Seq(((Location(x._1.toDouble, x._2.toDouble), fahrToCelcius(words(4).toDouble))))
          case _       => List()
        }
      } else { println(s"error in line (${year}): " + line);List()}
    }).groupByKey().mapValues(temps => temps.sum / temps.size).collect().toSeq
    println(s"Average temperature for ${year} calculated")
    res
  }

  //  def locationYearlyAverageRecordsDf(records: Iterable[(LocalDate, Location, Temperature)]): Iterable[(Location, Temperature)] = {
  //    val inStruc = StructType(List(
  //      //new StructField("Year", IntegerType, true),
  //      //new StructField("Month", IntegerType, true),
  //      //new StructField("Day", IntegerType, true),
  //      new StructField("Latitude", DoubleType, true),
  //      new StructField("Longitude", DoubleType, true),
  //      new StructField("Temperature", DoubleType, true)).toArray)
  //
  //    val recDf = spark.createDataFrame(spark.sparkContext.parallelize(records.toSeq.map { case (dat, loc, temp) => Row(loc.lat, loc.lon, temp) }), inStruc)
  //    val outDf = recDf.groupBy("Latitude", "Longitude").agg(avg("Temperature").as("Temperature"))
  //    //output conversion
  //    outDf.collect().map(r => (Location(r.getAs[Double]("Latitude"), r.getAs[Double]("Longitude")), r.getAs[Double]("Temperature")))
  //  }

}
