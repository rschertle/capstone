package observatory

import org.apache.hadoop.fs.FileSystem
import org.apache.hadoop.fs.Path
import org.apache.spark.sql.types.StructType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.DoubleType
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.Row
import org.apache.spark.sql.Dataset
import org.apache.spark.sql.Dataset

object Main extends App {
  import org.apache.spark.sql.SparkSession
  import org.apache.log4j.{ Level, Logger }
  Logger.getLogger("org.apache.spark").setLevel(Level.WARN)

  //TODO: merge genImage functions
  //TODO: stations file reading for each thread

  case class yearAVGTemp(year: Year, avgTemp: GridLocation => Temperature)

  val StationsFile = "hdfs:///tmp/stations.csv" //HDFS
  //  val StationsFile = getClass.getResource("/stations.csv").toURI.toString() //local file system /src/main/resources
  //  def tempFile(year: Year): String = getClass.getResource(s"/${year}.csv").toURI.toString() //local file system /src/main/resources
  def tempFile(year: Year): String = s"hdfs:///tmp/${year}.csv" //HDFS
  def trgPath(subdir: String, year: Year) = s"hdfs:///tmp/target/${subdir}/${year}/" //HDFS

  val spark: SparkSession =
    SparkSession
      .builder()
      .appName("capstone")
      .config("spark.master", "local")
      .enableHiveSupport()
      .getOrCreate()

  import spark.sqlContext.implicits._

  def genAVGImage(stationsMap: Map[(String, String), (String, String)])(year: Year): yearAVGTemp = {

    def genImage(year: Int, tile: Tile, gridMap: GridLocation => Temperature): Unit = {
      val img = Visualization2.visualizeGrid(gridMap, Visualization.temperatureColors, tile, 128).scaleTo(256, 256)
      //      new java.io.File(trgPath("temperatures", year) + s"${tile.zoom}").mkdirs()
      //      img.output(new java.io.File(trgPath("temperatures", year) + s"${tile.zoom}/${tile.x}-${tile.y}.png"))
      val fs = FileSystem.get(spark.sparkContext.hadoopConfiguration);
      fs.mkdirs(new Path(trgPath("temperatures", year) + s"${tile.zoom}"))
      fs.create(new Path(trgPath("temperatures", year) + s"${tile.zoom}/${tile.x}-${tile.y}.png")).write(img.bytes)
      println(s"File /$year/${tile.zoom}/${tile.x}-${tile.y}.png created")
      ()
    }

    val avgTempYear = Manipulation.toGrid(Extraction.locationYearlyAverageRecords(year, stationsMap, tempFile(year)), 3).get _

    println(s"Grid for year ${year} loaded")

    Interaction.generateTiles[GridLocation => Temperature](
      Seq((year, avgTempYear)),
      genImage _,
      (0 to 3))
    println(s"AVG-Images for year $year finished")
    yearAVGTemp(year, avgTempYear)

  }

  def genDEVImage(x1: yearAVGTemp, x2: yearAVGTemp): Unit = {

    def deviation(x1: GridLocation => Temperature, x2: GridLocation => Temperature): GridLocation => Temperature = {
      x => x1(x) - x2(x)
    }

    def genImage(year: Int, tile: Tile, gridMap: GridLocation => Temperature): Unit = {
      val img = Visualization2.visualizeGrid(gridMap, Visualization2.deviationColors, tile, 128).scaleTo(256, 256)
      //      new java.io.File(trgPath("deviations", year) + s"${tile.zoom}").mkdirs()
      //      img.output(new java.io.File(trgPath("deviations", year) + s"${tile.zoom}/${tile.x}-${tile.y}.png"))
      val fs = FileSystem.get(spark.sparkContext.hadoopConfiguration);
      fs.mkdirs(new Path(trgPath("deviations", year) + s"${tile.zoom}"))
      fs.create(new Path(trgPath("deviations", year) + s"${tile.zoom}/${tile.x}-${tile.y}.png")).write(img.bytes)
      ()
    }

    require(x1.year == x2.year - 1 && x1.year > 0 && x2.year > 0)

    Interaction.generateTiles[GridLocation => Temperature](
      Seq((x2.year, deviation(x1.avgTemp, x2.avgTemp))),
      genImage _,
      (0 to 3))
    println(s"DEV-Images for year ${x2.year} finished")
  }

  /**
   * seqop function of the aggregation implementing a neighbouring algorithm
   * @param a             		function applied to all elements
   * @param f     						accumulator function
   * @param acc 							intermediate results
   * @param year
   * @return A new intermediate result by applying a to year, f to acc and a(year) and finally combining the results
   */
  def accTempYear(a: Year => yearAVGTemp, f: (yearAVGTemp, yearAVGTemp) => Unit)(
    acc: (Option[yearAVGTemp], Option[yearAVGTemp]), year: Year): (Option[yearAVGTemp], Option[yearAVGTemp]) = {
    require(acc match { case (Some(_), None) => false; case _ => true })
    val aVG = a(year)
    acc match {
      case (None, None)       => (acc._1, Some(aVG))
      case (None, Some(y))    => { f(y, aVG); (Some(y), Some(aVG)) }
      case (Some(x), Some(y)) => { f(y, aVG); (Some(x), Some(aVG)) }
      case _                  => ??? //Should never happen
    }
  }

  /**
   * combop function of the aggregation implementing a neighbouring algorithm
   */
  def combTempYear(f: (yearAVGTemp, yearAVGTemp) => Unit)(
    x1: (Option[yearAVGTemp], Option[yearAVGTemp]),
    x2: (Option[yearAVGTemp], Option[yearAVGTemp])): (Option[yearAVGTemp], Option[yearAVGTemp]) = {
    (x1, x2) match {
      case ((None, Some(x)), (None, Some(y)))      => { f(x, y); (Some(x), Some(y)) }
      case ((None, Some(x)), (Some(y1), Some(y2))) => { f(x, y1); (Some(x), Some(y2)) }
      case ((x1, Some(x2)), (None, Some(y)))       => { f(x2, y); (x1, Some(y)) }
    }
  }

  val stationsMap = Extraction.parseStationsFile(StationsFile)
  println(s"Station data loaded")

  val yearAVGSeq = (1975 to 1988).toVector.par //average temperature tile generation
  val yearDEVSeq = (1989 to 2015).toVector.par //deviation temperature tile generation

  /**
   * Generate grids, tiles and images for the average temperatures of the years and save them (locally or in HDFS)
   */
  //  println("Step 1: Generate Images for average temperatures")
  //  yearAVGSeq.map(year => genAVGImage(stationsMap)(year))

  /**
   * Generate grids, tiles and images for the temperature deviations of the years and save them (locally or in HDFS)
   */
  //  println("Step 2: Generate Images for temperature deviations")
  //  yearDEVSeq.aggregate[(Option[yearAVGTemp], Option[yearAVGTemp])]((None, None))(accTempYear(genAVGImage(stationsMap), genDEVImage), combTempYear(genDEVImage))

  /**
   * Save the stationsMap in Hive
   */
  //Several options to create the DataFrame
  def stationsDf(option: Int): Dataset[Row] = {
    option match {
      case 1 =>
        //option 1:
        spark.createDataFrame(spark.sparkContext.parallelize(stationsMap.map { case ((a, b), (c, d)) => Row(a, b, c.toDouble, d.toDouble) }.toSeq), new StructType(List(
          new StructField("STN", StringType, true),
          new StructField("WBAN", StringType, true),
          new StructField("Latitude", DoubleType, true),
          new StructField("Longitude", DoubleType, true)).toArray))
      case 2 =>
        //option 2:
        stationsMap.map { case ((a, b), (c, d)) => (a, b, c.toDouble, d.toDouble) }.toSeq.toDF("STN", "WBAN", "Latitude", "Longitude")
      case _ => ???
    }
  }

  //Several options to store a DataFrame in Hive
  def hiveStoreStations(df: Dataset[Row], option: Int): Unit = {
    option match {
      case 1 =>
        //option 1:
        df.write.mode("overwrite").format("ORC").saveAsTable("weatherstations")
      case 2 =>
        //option 2:
        spark.sqlContext.sql("DROP TABLE IF EXISTS weatherstations")
        spark.sqlContext.sql("CREATE TABLE weatherstations (stn STRING, wban STRING, latitude DOUBLE, longitude1 DOUBLE) stored as orc")
        df.createOrReplaceTempView("weather")
        spark.sqlContext.sql("INSERT INTO weatherstations SELECT * FROM weather")
        ()
      case 3 =>
        //option 3:
        spark.sqlContext.sql("DROP TABLE IF EXISTS weatherstations")
        df.createOrReplaceTempView("weather")
        spark.sqlContext.sql("CREATE TABLE weatherstations AS SELECT * FROM weather")
        ()
      case _ => ???
    }
  }
  
  hiveStoreStations(stationsDf(2),3)

}
