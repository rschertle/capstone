package observatory

import com.sksamuel.scrimage.{ Image, Pixel }

/**
 * 2nd milestone: basic visualization
 */
object Visualization {

  val temperatureColors: Seq[(Temperature, Color)] = Seq(
    (-60, Color(0, 0, 0)),
    (-50, Color(33, 0, 107)),
    (-27, Color(255, 0, 255)),
    (-15, Color(0, 0, 255)),
    (0, Color(0, 255, 255)),
    (12, Color(255, 255, 0)),
    (32, Color(255, 0, 0)),
    (60, Color(255, 255, 255)))

  val interpolatePower = 6

  /**
   * @param temperatures Known temperatures: pairs containing a location and the temperature at this location
   * @param location Location where to predict the temperature
   * @return The predicted temperature at `location`
   */
  def predictTemperature(temperatures: Iterable[(Location, Temperature)], location: Location): Temperature = {

    def recIdw(temps: Iterator[(Location, Temperature)], location: Location)(weight: Double, weightProd: Double): Double = {
      if (temps.hasNext) {
        val currLocTemp = temps.next()
        val distance = currLocTemp._1.greatCircleDistance(location)
        if (distance < 1.0) currLocTemp._2
        else {
          val currWeight = 1.0 / math.pow(distance, interpolatePower);
          recIdw(temps, location)(weight + currWeight, weightProd + (currWeight * currLocTemp._2))
        }
      } else weightProd / weight
    }
    recIdw(temperatures.iterator, location)(0.0, 0.0)

    //    //Alternative:
    //    val distTempPair = temperatures.map { case (loc, temp) => (loc.greatCircleDistance(location), temp) }
    //    val closePoint = distTempPair.minBy { case (dist, temp) => dist }
    //
    //    if (closePoint._1 < 1.0) closePoint._2
    //    else {
    //      val weightedDist = distTempPair.map {
    //        case (dist, temp) => {
    //          val weight = 1.0 / math.pow(dist, interpolatePower);
    //          (weight, weight * temp)
    //        }
    //      }
    //        .reduce { (x, y) => (x._1 + y._1, x._2 + y._2) }
    //      weightedDist._2 / weightedDist._1
    //    }
  }

  /**
   * @param points Pairs containing a value and its associated color
   * @param value The value to interpolate
   * @return The color that corresponds to `value`, according to the color scale defined by `points`
   */
  def interpolateColor(points: Iterable[(Temperature, Color)], value: Temperature): Color = {
    require(points.size > 0)
    val sortByTemp = points.toSeq.sortWith(_._1 < _._1)
    val startIdx = sortByTemp.prefixLength { case (temp, col) => temp <= value }

    if (points.size == 1) sortByTemp(0)._2
    else if (startIdx <= 0) sortByTemp(0)._2
    else if (startIdx >= sortByTemp.size) sortByTemp(sortByTemp.size - 1)._2
    else interpolateColor(sortByTemp(startIdx - 1), sortByTemp(startIdx), value)

  }

  def interpolateValue(x0: (Double, Int), x1: (Double, Int), value: Double): Int = {
    math.round(
      (x0._2.toDouble * (x1._1 - value) + x1._2.toDouble * (value - x0._1))
        / (x1._1 - x0._1)).toInt
  }

  def interpolateColor(x1: (Temperature, Color), x2: (Temperature, Color), value: Temperature): Color = {

    Color(
      interpolateValue((x1._1, x1._2.red), (x2._1, x2._2.red), value),
      interpolateValue((x1._1, x1._2.green), (x2._1, x2._2.green), value),
      interpolateValue((x1._1, x1._2.blue), (x2._1, x2._2.blue), value))
  }

  /**
   * @param temperatures Known temperatures
   * @param colors Color scale
   * @return A 360×180 image where each pixel shows the predicted temperature at its location
   */
  def visualize(temperatures: Iterable[(Location, Temperature)], colors: Iterable[(Temperature, Color)]): Image = {
    val colorArr = for (y <- (0 until 180); x <- (0 until 360))
      yield interpolateColor(colors, predictTemperature(temperatures, Location(90 - y, -180 + x)))

    Image(360, 180, colorArr.map(color => Pixel(color.red, color.green, color.blue, 255)).toArray)
  }

}

