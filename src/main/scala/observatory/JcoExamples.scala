package observatory

import com.sap.conn.jco._
import java.util.concurrent.CountDownLatch
import scala.collection.mutable.ListBuffer
import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructType
import org.apache.spark.sql.types.DecimalType
import org.apache.spark.sql.types.ByteType
import org.apache.spark.sql.types.DateType
import org.apache.spark.sql.types.TimestampType
import org.apache.spark.sql.types.IntegerType
import org.apache.spark.sql.types.LongType
import org.apache.spark.sql.types.DoubleType
import org.apache.spark.sql.types.DataType
import org.apache.spark.sql.types.ShortType
import scala.collection.mutable.Map
import java.util.Calendar

object JcoExamples extends App {

  val AbapAs = "ABAP_AS_WITHOUT_POOL";
  val AbapAsPooled = "ABAP_AS_WITH_POOL";

  //  step1Connect();
  //  step2ConnectUsingPool();
  //  step3SimpleCall();
  //  step3WorkWithStructure()
  //  step4WorkWithTable();
  //  step4SimpleStatefulCalls();
  bIWCall();

  /**
   * This example demonstrates the destination concept introduced with JCO 3.
   * The application does not deal with single connections anymore. Instead
   * it works with logical destinations like ABAP_AS which separates
   * the application logic from technical configuration.
   * @throws JCoException
   */
  def step1Connect(): Unit =
    {
      val destination: JCoDestination = JCoDestinationManager.getDestination(AbapAs);
      System.out.println("Attributes:");
      System.out.println(destination.getAttributes());
      System.out.println();

      val destination2 = JCoDestinationManager.getDestination(AbapAs);
      System.out.println("Attributes:");
      System.out.println(destination2.getAttributes());
      System.out.println();
    }

  /**
   * This example uses a connection pool. However, the implementation of
   * the application logic is still the same. Creation of pools and pool management
   * are handled by the JCo runtime.
   *
   * @throws JCoException
   */
  def step2ConnectUsingPool(): Unit =
    {
      val destination: JCoDestination = JCoDestinationManager.getDestination(AbapAsPooled);
      destination.ping();
      System.out.println("Attributes:");
      System.out.println(destination.getAttributes());
      System.out.println();
    }

  /**
   * The following example executes a simple RFC function STFC_CONNECTION.
   * In contrast to JCo 2 you do not need to take care of repository management.
   * JCo 3 manages the repository caches internally and shares the available
   * function metadata as much as possible.
   * @throws JCoException
   */
  def step3SimpleCall(): Unit =
    {
      //JCoDestination is the logic address of an ABAP system and ...
      val destination: JCoDestination = JCoDestinationManager.getDestination(AbapAsPooled);
      // ... it always has a reference to a metadata repository
      val function: JCoFunction = destination.getRepository().getFunction("STFC_CONNECTION");
      if (function == null)
        throw new RuntimeException("BAPI_COMPANYCODE_GETLIST not found in SAP.");

      //JCoFunction is container for function values. Each function contains separate
      //containers for import, export, changing and table parameters.
      //To set or get the parameters use the APIS setValue() and getXXX().
      function.getImportParameterList().setValue("REQUTEXT", "Hello SAP");

      try {
        //execute, i.e. send the function to the ABAP system addressed
        //by the specified destination, which then returns the function result.
        //All necessary conversions between Java and ABAP data types
        //are done automatically.
        function.execute(destination);
      } catch {
        case e: AbapException =>
          println(e.toString());
          return ;
      }

      System.out.println("STFC_CONNECTION finished:");
      System.out.println(" Echo: " + function.getExportParameterList().getString("ECHOTEXT"));
      System.out.println(" Response: " + function.getExportParameterList().getString("RESPTEXT"));
      System.out.println();
    }

  /**
   * ABAP APIs often uses complex parameters. This example demonstrates
   * how to read the values from a structure.
   * @throws JCoException
   */
  def step3WorkWithStructure(): Unit =
    {
      val destination: JCoDestination = JCoDestinationManager.getDestination(AbapAsPooled);
      val function: JCoFunction = destination.getRepository().getFunction("RFC_SYSTEM_INFO");
      if (function == null)
        throw new RuntimeException("RFC_SYSTEM_INFO not found in SAP.");

      try {
        function.execute(destination);
      } catch {
        case e: AbapException =>
          println(e.toString());
          return ;
      }

      val exportStructure: JCoStructure = function.getExportParameterList().getStructure("RFCSI_EXPORT");
      println(s"System info for ${destination.getAttributes().getSystemID()}:");

      //The structure contains some fields. The loop just prints out each field with its name.
      for (i <- 0 until exportStructure.getMetaData().getFieldCount()) {
        println(exportStructure.getMetaData().getName(i) + ":\t" + exportStructure.getString(i));
      }
      println();

      //JCo still supports the JCoFields, but direct access via getXXX is more efficient as field iterator
      println("The same using field iterator: \nSystem info for " + destination.getAttributes().getSystemID() + ":\n");
      exportStructure.forEach(field => println(field.getName() + ":\t" + field.getString()))

      println();
    }

  /**
   * A slightly more complex example than before. Query the companies list
   * returned in a table and then obtain more details for each company.
   * @throws JCoException
   */
  def step4WorkWithTable(): Unit =
    {
      val destination: JCoDestination = JCoDestinationManager.getDestination(AbapAsPooled);
      val function: JCoFunction = destination.getRepository().getFunction("BAPI_COMPANYCODE_GETLIST");
      if (function == null)
        throw new RuntimeException("BAPI_COMPANYCODE_GETLIST not found in SAP.");

      try {
        function.execute(destination);
      } catch {
        case e: AbapException =>
          System.out.println(e.toString());
          return ;
      }

      val returnStructure: JCoStructure = function.getExportParameterList().getStructure("RETURN");
      if (!(returnStructure.getString("TYPE").equals("") || returnStructure.getString("TYPE").equals("S"))) {
        throw new RuntimeException(returnStructure.getString("MESSAGE"));
      }

      val codes: JCoTable = function.getTableParameterList().getTable("COMPANYCODE_LIST");
      for (i <- 0 until codes.getNumRows()) {
        codes.setRow(i);
        println(codes.getString("COMP_CODE") + '\t' + codes.getString("COMP_NAME"));
      }

      //move the table cursor to first row
      codes.firstRow();
      for (i <- 0 until codes.getNumRows()) {
        val function = destination.getRepository().getFunction("BAPI_COMPANYCODE_GETDETAIL");
        if (function == null)
          throw new RuntimeException("BAPI_COMPANYCODE_GETDETAIL not found in SAP.");

        function.getImportParameterList().setValue("COMPANYCODEID", codes.getString("COMP_CODE"));

        //We do not need the addresses, so set the corresponding parameter to inactive.
        //Inactive parameters will be  either not generated or at least converted.
        function.getExportParameterList().setActive("COMPANYCODE_ADDRESS", false);

        try {
          function.execute(destination);
        } catch {
          case e: AbapException =>
            System.out.println(e.toString());
            return ;
        }

        val returnStructure = function.getExportParameterList().getStructure("RETURN");
        if (!(returnStructure.getString("TYPE").equals("") ||
          returnStructure.getString("TYPE").equals("S") ||
          returnStructure.getString("TYPE").equals("W"))) {
          throw new RuntimeException(returnStructure.getString("MESSAGE"));
        }

        val detail: JCoStructure = function.getExportParameterList().getStructure("COMPANYCODE_DETAIL");

        println(detail.getString("COMP_CODE") + '\t' +
          detail.getString("COUNTRY") + '\t' +
          detail.getString("CITY"));
        codes.nextRow()
      } //for
    }

  /**
   * this example shows the "simple" stateful call sequence. Since all calls belonging to one
   * session are executed within the same thread, the application does not need
   * to take into account the SessionReferenceProvider. MultithreadedExample.java
   * illustrates the more complex scenario, where the calls belonging to one session are
   * executed in different threads.
   *
   * Note: this example uses Z_GET_COUNTER and Z_INCREMENT_COUNTER. Most ABAP systems
   * contain function modules GET_COUNTER and INCREMENT_COUNTER that are not remote-enabled.
   * Copy these functions to Z_GET_COUNTER and Z_INCREMENT_COUNTER (or implement as wrapper)
   * and declare them to be remote enabled.
   * @throws JCoException
   */
  def step4SimpleStatefulCalls(): Unit =
    {
      val destination: JCoDestination = JCoDestinationManager.getDestination(AbapAsPooled);
      val incrementCounterTemplate: JCoFunctionTemplate = destination.getRepository().getFunctionTemplate("Z_INCREMENT_COUNTER");
      val getCounterTemplate: JCoFunctionTemplate = destination.getRepository().getFunctionTemplate("Z_GET_COUNTER");
      if (incrementCounterTemplate == null || getCounterTemplate == null)
        throw new RuntimeException("This example cannot run without Z_INCREMENT_COUNTER and Z_GET_COUNTER functions");

      val threadCount = 5;
      val loops = 5;
      val startSignal: CountDownLatch = new CountDownLatch(threadCount);
      val doneSignal: CountDownLatch = new CountDownLatch(threadCount);

      object worker extends Runnable {
        def run() =
          {
            startSignal.countDown();
            try {
              //wait for other threads
              startSignal.await();

              val dest: JCoDestination = JCoDestinationManager.getDestination(AbapAsPooled);
              JCoContext.begin(dest);
              try {
                for (i <- 0 until loops) {
                  val incrementCounter: JCoFunction = incrementCounterTemplate.getFunction();
                  incrementCounter.execute(dest);
                }
                val getCounter: JCoFunction = getCounterTemplate.getFunction();
                getCounter.execute(dest);

                val remoteCounter: Int = getCounter.getExportParameterList().getInt("GET_VALUE");
                println("Thread-" + Thread.currentThread().getId() +
                  " finished. Remote counter has " + (if (loops == remoteCounter) "correct" else "wrong") +
                  " value [" + remoteCounter + "]");
              } finally {
                JCoContext.end(dest);
              }
            } catch {
              case e: Exception =>
                println("Thread-" + Thread.currentThread().getId() + " ends with exception " + e.toString());
            }

            doneSignal.countDown();
          }
      };

      for (i <- 0 until threadCount) {
        new Thread(worker).start();
      }

      try {
        doneSignal.await();
      } catch {
        case _: Throwable => ()
      }

    }

  def bIWCall(): Unit = {

    //TODO: Evaluate returned bapi messages
    //TODO: Implement packetizer

    val spark: SparkSession =
      SparkSession
        .builder()
        .appName("capstone")
        .config("spark.master", "local")
        //        .enableHiveSupport()
        .getOrCreate()

    import spark.sqlContext.implicits._

    //    def packetizer(): = {
    //
    //    }

    case class DSOIobj(POSIT: Int, KEYFLAG: String, IOBJNM: String, IOBJTP: String, LENG: Int, DECIMALS: Int)
    var iobjNmMap: Map[String, DSOIobj] = Map.empty

    val IProv: String = "Z_DSO_L1"
    val MaxRows: Int = 200000
    val SelectAllIobj = false

    val destination: JCoDestination = JCoDestinationManager.getDestination(AbapAsPooled);

    ////BAPIs:
    //InfoCube 	          BAPI_CUBE_GETDETAIL
    //InfoObject Catalog 	BAPI_IOBC_GETDETAIL
    //InfoObject 	        BAPI_IOBJ_GETDETAIL
    //InfoPackage 	      BAPI_IPAK_GETDETAIL
    //InfoSet 	          BAPI_ISET_GETDETAIL
    //DSO 	              BAPI_ODSO_GETDETAIL
    //MultiProvider 	    BAPI_MPRO_GETDETAIL

    val dSODetailsRFC = destination.getRepository().getFunction("BAPI_ODSO_GETDETAIL");
    if (dSODetailsRFC == null)
      throw new RuntimeException("BAPI_ODSO_GETDETAIL not found in SAP.");
    //IMPORT:
    //ODSOBJECT	LIKE	BAPI6116-ODSOBJECT
    //SELECTALLINFOOBJECTS	LIKE	BAPI6116XX-ALLINFOOBJECTS
    //MAXROWS	LIKE	BAPI6116XX-MAXROWS
    //CODEPAGE	LIKE	BAPI6116XX-CODEPAGE
    //UNICODE	LIKE	BAPI6116XX-ALLINFOOBJECTS
    //EXPORT:
    //NUMROWS	LIKE	BAPI6116XX-NUMROWS
    //RETURN	LIKE	BAPIRET2
    //TABLES:
    //INFOOBJECTLIST	LIKE	BAPI6116IOLS
    //SELECTIONCRITERIA	LIKE	BAPI6116SLIO
    //ORDERBY	LIKE	BAPI6116IOLS
    //RESULTDATA	LIKE	BAPI6116DAUC
    //DATALAYOUT	LIKE	BAPI6116DALO
    val function: JCoFunction = destination.getRepository().getFunction("BAPI_ODSO_READ_DATA_UC");
    if (function == null)
      throw new RuntimeException("BAPI_ODSO_READ_DATA_UC not found in SAP.");

    dSODetailsRFC.getImportParameterList().setValue("OBJVERS", "A")
    dSODetailsRFC.getImportParameterList().setValue("ODSOBJECT", IProv)

    try {
      dSODetailsRFC.execute(destination);
    } catch {
      case e: AbapException =>
        System.out.println(e.toString());
        return ;
    }
    //get detailed information about dso infoobjects
    val dSOIobjs: JCoTable = dSODetailsRFC.getTableParameterList().getTable("INFOOBJECTS");
    for (i <- 0 until dSOIobjs.getNumRows) {
      dSOIobjs.setRow(i);
      val iOBJDetailsRFC = destination.getRepository().getFunction("BAPI_IOBJ_GETDETAIL");
      if (iOBJDetailsRFC == null)
        throw new RuntimeException("BAPI_IOBJ_GETDETAIL not found in SAP.");
      iOBJDetailsRFC.getImportParameterList().setValue("INFOOBJECT", dSOIobjs.getString("INFOOBJECT"))
      try {
        iOBJDetailsRFC.execute(destination);
      } catch {
        case e: AbapException =>
          System.out.println(e.toString());
          return ;
      }
      val iobjDetails: JCoStructure = iOBJDetailsRFC.getExportParameterList.getStructure("DETAILS")
      iobjNmMap += ((
        iobjDetails.getString("INFOOBJECT"),
        DSOIobj(dSOIobjs.getInt("POSIT"), dSOIobjs.getString("KEYFLAG"), iobjDetails.getString("FIELDNM"), dSOIobjs.getString("IOBJTP"), iobjDetails.getInt("LENG"), iobjDetails.getInt("DECIMALS"))))
    }

    val itabIobjList: JCoTable = function.getTableParameterList().getTable("INFOOBJECTLIST");
    val itabRange: JCoTable = function.getTableParameterList().getTable("SELECTIONCRITERIA");
    val itabOrder: JCoTable = function.getTableParameterList().getTable("ORDERBY");

    //build projection/select list
    if (!SelectAllIobj)
      iobjNmMap.map {
        case (key, value) => { itabIobjList.appendRow(); itabIobjList.setValue("INFOOBJECT", key) }
      }

    //    //build filter/where list
    //    itabRange.appendRow();
    //    itabRange.setValue("IOBJNM", "Z_COUNTER")
    //    itabRange.setValue("SIGN", "I")
    //    itabRange.setValue("OPT", "EQ")
    //    itabRange.setValue("LOW", "000000000033")

    function.getImportParameterList().setValue("ODSOBJECT", IProv)
    function.getImportParameterList().setValue("SELECTALLINFOOBJECTS", if (SelectAllIobj) "X" else "")
    function.getImportParameterList().setValue("MAXROWS", MaxRows)
    //      function.getImportParameterList().setValue("CODEPAGE", "XXXX")
    function.getImportParameterList().setValue("UNICODE", "X")

    function.getExportParameterList().setActive("RETURN", false);
    function.getExportParameterList().setActive("NUMROWS", false);
    function.getTableParameterList().setActive("SELECTIONCRITERIA", false);
    function.getTableParameterList().setActive("ORDERBY", false);

    try {
      println("Extraction start: " + Calendar.getInstance().getTime)
      function.execute(destination);

    } catch {
      case e: AbapException =>
        System.out.println(e.toString());
        return ;
    }

    //build resulting data structure
    val dataLayoutTab = function.getTableParameterList().getTable("DATALAYOUT")
    val struc: JCoStructure = com.sap.conn.jco.JCo.createStructure(destination.getRepository().getStructureDefinition("/BIC/A" + IProv + "00"))
    var typeList = new ListBuffer[StructField]()
    for (i <- 0 until dataLayoutTab.getNumRows()) {
      dataLayoutTab.setRow(i);
      val fld = dataLayoutTab.getString("INFOOBJECT")
      val fldDetails = iobjNmMap.get(fld).get
      val datType: DataType = struc.getRecordMetaData.getType(fldDetails.IOBJNM) match {
        //Use CharType in Spark 2.1.1
        case com.sap.conn.jco.JCoMetaData.TYPE_NUM |
          com.sap.conn.jco.JCoMetaData.TYPE_CHAR |
          com.sap.conn.jco.JCoMetaData.TYPE_STRING |
          com.sap.conn.jco.JCoMetaData.TYPE_XSTRING => StringType
        case com.sap.conn.jco.JCoMetaData.TYPE_BYTE => ByteType
        case com.sap.conn.jco.JCoMetaData.TYPE_BCD |
          com.sap.conn.jco.JCoMetaData.TYPE_DECF16 |
          com.sap.conn.jco.JCoMetaData.TYPE_DECF34 => DecimalType(fldDetails.LENG, fldDetails.DECIMALS)
        case com.sap.conn.jco.JCoMetaData.TYPE_DATE => DateType
        case com.sap.conn.jco.JCoMetaData.TYPE_TIME => TimestampType
        case com.sap.conn.jco.JCoMetaData.TYPE_INT  => IntegerType
        case com.sap.conn.jco.JCoMetaData.TYPE_INT1 |
          com.sap.conn.jco.JCoMetaData.TYPE_INT2 => ShortType
        case com.sap.conn.jco.JCoMetaData.TYPE_INT8  => LongType
        case com.sap.conn.jco.JCoMetaData.TYPE_FLOAT => DoubleType
        case _                                       => ???
      }
      typeList += new StructField(fldDetails.IOBJNM, datType, true)
    }
    val strucType = new StructType(typeList.toArray)

    //split result-string to individual field values
    val returnTab = function.getTableParameterList().getTable("RESULTDATA")
    var line: String = ""
    var lines = new ListBuffer[String]()
    var byteSize = 0
    for (i <- 0 until returnTab.getNumRows()) {
      returnTab.setRow(i);
      if (returnTab.getChar("CONTINUATION") == 'X') line = line + returnTab.getString("DATA") else { if (i != 0) lines += line; line = returnTab.getString("DATA"); }
    }
    if (returnTab.getNumRows() > 0) {
      lines += line
    }

    //conversion to proper data types
    val lineList = lines.map(currLine => {
      struc.clear
      Row.fromSeq(for (j <- 0 until dataLayoutTab.getNumRows()) yield {
        dataLayoutTab.setRow(j);
        if (currLine.size >= dataLayoutTab.getInt("OFFSET") + dataLayoutTab.getInt("LENGTH")) {
          val currFld = currLine.substring(dataLayoutTab.getInt("OFFSET"), dataLayoutTab.getInt("OFFSET") + dataLayoutTab.getInt("LENGTH"))
          val currIobjNm = iobjNmMap.get(dataLayoutTab.getString("INFOOBJECT")).get.IOBJNM
          struc.setValue(currIobjNm, currFld)
          //use JCo to convert the data types
          dataLayoutTab.getChar("TYPE") match {
            case 'N' | 'C' | 'g' | 'V'                       => struc.getString(currIobjNm)
            case 'X' | 'y'                                   => struc.getByte(currIobjNm)
            case 'P' | 'a' | 'e'                             => struc.getBigDecimal(currIobjNm)
            case 'D'                                         => new java.sql.Date(struc.getDate(currIobjNm).getTime)
            case 'T'                                         => new java.sql.Timestamp(struc.getTime(currIobjNm).getTime)
            case 'I' | 'b' | 's'                             => struc.getInt(currIobjNm)
            case 'F'                                         => struc.getDouble(currIobjNm)
            case 'u' | 'v' | 'h' | 'r' | 'l' | 'j' | 'k' | _ => ???
          }
        }
      })
    })
    println("Processing end: " + Calendar.getInstance().getTime)
    spark.createDataFrame(spark.sparkContext.parallelize(lineList), strucType).show()
    println("Returned lines: " + returnTab.getNumRows())
  }

}