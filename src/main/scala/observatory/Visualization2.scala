package observatory

import com.sksamuel.scrimage.{ Image, Pixel }

/**
 * 5th milestone: value-added information visualization
 */
object Visualization2 {

  val tileSize = 256

  val deviationColors: Seq[(Temperature, Color)] = Seq(
    (-7.0, Color(0, 0, 255)),
    (-2.0, Color(0, 255, 255)),
    (0.0, Color(255, 255, 255)),
    (2.0, Color(255, 255, 0)),
    (4.0, Color(255, 0, 0)),
    (7.0, Color(0, 0, 0)))

  /**
   * @param point (x, y) coordinates of a point in the grid cell
   * @param d00 Top-left value
   * @param d01 Bottom-left value
   * @param d10 Top-right value
   * @param d11 Bottom-right value
   * @return A guess of the value at (x, y) based on the four known values, using bilinear interpolation
   *         See https://en.wikipedia.org/wiki/Bilinear_interpolation#Unit_Square
   */
  def bilinearInterpolation(
    point: CellPoint,
    d00:   Temperature,
    d01:   Temperature,
    d10:   Temperature,
    d11:   Temperature): Temperature = {
    val xInv = 1.0 - point.x
    val yInv = 1.0 - point.y
    d00 * xInv * yInv + d10 * point.x * yInv + d01 * xInv * point.y + d11 * point.x * point.y
  }

  /**
   * @param grid Grid to visualize
   * @param colors Color scale to use
   * @param tile Tile coordinates to visualize
   * @return The image of the tile at (x, y, zoom) showing the grid using the given color scale
   */
  def visualizeGrid(
    grid:   GridLocation => Temperature,
    colors: Iterable[(Temperature, Color)],
    tile:   Tile): Image = {
    visualizeGrid(grid, colors, tile, tileSize)
//    val latRange = (-89 to 90)
//    val lonRange = (-180 to 179)
//    val keys = (for (lat <- latRange; lon <- lonRange) yield GridLocation(lat, lon))
//    Interaction.tile(keys.map(x => (x.toLocation, grid(x))), colors, tile)
  }

  def visualizeGrid(
    grid:   GridLocation => Temperature,
    colors: Iterable[(Temperature, Color)],
    tile:   Tile,
    size:   Int): Image = {
    val tileScale = (size * tile.x, size * tile.y, tile.zoom + (math.log(size) / math.log(2)).toInt)
    val coordIter = for (y <- (0 until size); x <- (0 until size)) yield (x, y)
    val colorArr = coordIter.map {
      case (x, y) => Visualization.interpolateColor(
        colors,
        {
          val pixelLoc = Interaction.tileLocation(Tile(tileScale._1 + x, tileScale._2 + y, tileScale._3))
          val refPoint = GridLocation(pixelLoc.lat.ceil.toInt, pixelLoc.lon.floor.toInt)
          val res = bilinearInterpolation(
            pixelLoc.toCellPoint(),
//            grid(GridLocation(pixelLoc.lat.floor.toInt, pixelLoc.lon.floor.toInt)),
//            grid(GridLocation(pixelLoc.lat.ceil.toInt, pixelLoc.lon.floor.toInt)),
//            grid(GridLocation(pixelLoc.lat.floor.toInt, pixelLoc.lon.ceil.toInt)),
//            grid(GridLocation(pixelLoc.lat.ceil.toInt, pixelLoc.lon.ceil.toInt)))
                      grid(refPoint),
                      grid(refPoint.decreaseLat),
                      grid(refPoint.increaseLon),
                      grid(refPoint.decreaseLat.increaseLon))

          //          //test output println
          //          if ((x == 255 || x == 0) && (y == 255 || y == 0)) {
          //            println("lat:" + pixelLoc.lat + " lon:" + pixelLoc.lon + " temp:" + res + " d00:(" + refPoint.lat + "," + refPoint.lon + ")" +
          //              " d01:(" + refPoint.increaseLon.lat + "," + refPoint.increaseLon.lon + ")" +
          //              " d10:(" + refPoint.decreaseLat.lat + "," + refPoint.decreaseLat.lon + ")" +
          //              " d11:(" + refPoint.decreaseLat.increaseLon.lat + "," + refPoint.decreaseLat.increaseLon.lon + ")" +
          //              " cellPoint:(" + pixelLoc.toCellPoint().x + "," + pixelLoc.toCellPoint().y + ")" +
          //              " pixel:(" + x + "," + y + ")")
          //          }
          res
        })
    }

    Image(size, size, colorArr.map(color => Pixel(color.red, color.green, color.blue, 128)).toArray)
  }

}
