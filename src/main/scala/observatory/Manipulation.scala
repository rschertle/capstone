package observatory

import scala.collection.parallel.ParMap
import scala.math.Numeric

/**
 * 4th milestone: value-added information
 */
object Manipulation {

  //immutable parallel grid
  class ParGrid[To : Numeric](val initial: ParMap[GridLocation, Option[To]]) {

    def set(loc: GridLocation, x: To): ParGrid[To] = {
      ParGrid(initial.updated(loc, Some(x)))
    }
    def get(loc: GridLocation): Option[To] = {
      initial.get(loc).getOrElse(None)
    }
    def get(default: To)(loc: GridLocation): To = {
      initial.get(loc).getOrElse(None).getOrElse(default)
    }
    def zip(grid: ParGrid[To], f: (To,To) => To): ParGrid[To] = {
      ParGrid(initial.map { case (loc, temp) => (loc, 
          (temp,grid.get(loc)) match {
        case (Some(x),Some(y)) => Some(f(x,y))
        case (None,Some(x)) => Some(x)
        case (Some(x),None) => Some(x)
        case _ => None})})
    }
    def mapValuesByKeys(f: GridLocation => To): ParGrid[To] = {
      ParGrid(initial.map{ case (loc, x) => (loc, Some(f(loc))) })
    }
    def mapValues(f: To => To): ParGrid[To] = {
      ParGrid(initial.mapValues{case Some(x) => Some(f(x)); case _ => None})
    }
  }
  
  object ParGrid {
    val latRange = (-89 to 90)
    val lonRange = (-180 to 179)
    val keys = (for (lat <- latRange; lon <- lonRange) yield GridLocation(lat, lon))
    
    def build[To : Numeric](initial: Map[GridLocation, To]) = new ParGrid(keys.map(loc => (loc,initial.get(loc))).toMap.par)
    def apply[To : Numeric](initial: ParMap[GridLocation, Option[To]]) = new ParGrid(initial)
    def empty[To : Numeric] = new ParGrid(keys.map(loc => (loc,None)).toMap.par)
  }

  //mutable memorization grid
  class MemoGrid[To: Numeric](val generator: GridLocation => To) {
    
    var gridMap: Map[GridLocation,To] = Map.empty

    def set(loc: GridLocation, x: To): Unit = {
      gridMap = gridMap.updated(loc, x)
    }
    def get(loc: GridLocation): To = {
      gridMap.get(loc) match {
        case Some(x) => x
        case _ => set(loc, generator(loc)); get(loc)
      }
    }
  }
  
  object MemoGrid {
    val latRange = (-89 to 90)
    val lonRange = (-180 to 179)
    
    def apply[To : Numeric](generator: GridLocation => To): MemoGrid[To] = new MemoGrid(generator)
    def withPreCalc[To : Numeric](generator: GridLocation => To, scale: Int = 1): MemoGrid[To] =  {
      require(90 % scale == 0)
      val div = 90 / scale
      val keys = (for (lat <- (-(div - 1) to div); lon <- (-(2 * div) to (2 * div) - 1)) yield (lat, lon)).par
      val grid = new MemoGrid(generator)
      println(s"Starting temperature prediction grid (${div * 2 - 1},${div * 4 - 1})")
      grid.gridMap = keys.map{ case (x,y) => (GridLocation(x,y),generator(GridLocation(x,y)))}.seq.toMap
      grid
    }
  }
  
  /**
   * @param temperatures Known temperatures
   * @return A function that, given a latitude in [-89, 90] and a longitude in [-180, 179],
   *         returns the predicted temperature at this location
   */
  def makeGrid(temperatures: Iterable[(Location, Temperature)]): GridLocation => Temperature = {
    toGrid(temperatures).get
    //AIO:
    ////toGrid(temperatures).get(0.0)
  }

  def toGrid(temperatures: Iterable[(Location, Temperature)], scale: Int = 1): MemoGrid[Temperature] = {
    MemoGrid.withPreCalc[Temperature]((gridLoc: GridLocation) => Visualization.predictTemperature(temperatures, Location(gridLoc.lat.toDouble, gridLoc.lon.toDouble)), scale)
  }
  
//  def toGrid(temperatures: Iterable[(Location, Temperature)]): ParGrid[Temperature] = {
//    //AIO solution:
//    ParGrid.empty[Temperature].mapValuesByKeys(gridLoc => Visualization.predictTemperature(temperatures, Location(gridLoc.lat.toDouble, gridLoc.lon.toDouble)))
//  }

  /**
   * @param temperaturess Sequence of known temperatures over the years (each element of the collection
   *                      is a collection of pairs of location and temperature)
   * @return A function that, given a latitude and a longitude, returns the average temperature at this location
   */
  def average(temperaturess: Iterable[Iterable[(Location, Temperature)]]): GridLocation => Temperature = {
    //only calculate average temp at requested location
    val grids = temperaturess.map(x => toGrid(x))
    x => {
      val temps = grids.map(grid => grid.get(x))
      temps.sum / temps.size
    }
    ////AIO solution:
    //temperaturess.map(x => toGrid(x)).reduce(_.zip(_,(x,y) => x + y)).mapValues( x => x / temperaturess.size.toDouble).get(0.0)
  }

  /**
   * @param temperatures Known temperatures
   * @param normals A grid containing the “normal” temperatures
   * @return A grid containing the deviations compared to the normal temperatures
   */
  def deviation(temperatures: Iterable[(Location, Temperature)], normals: GridLocation => Temperature): GridLocation => Temperature = {
    //only calculate deviation at requested location
    val grid = toGrid(temperatures)
    x => {
      //hint: grader seems to expect negative numbers
      //math.abs(grid.get(x) - normals(x))
      grid.get(x) - normals(x)
    }
    ////AIO solution:
    //toGrid(temperatures).zip(ParGrid.empty[Temperature].mapValuesByKeys(normals),(x,y) => math.abs(x - y)).get(0.0)
  }

}

