package observatory

import com.sksamuel.scrimage.{ Image, Pixel }

/**
 * 3rd milestone: interactive visualization
 */
object Interaction {

  val alpha = 127
  val tilesize = 256

  /**
   * @param tile Tile coordinates
   * @return The latitude and longitude of the top-left corner of the tile, as per http://wiki.openstreetmap.org/wiki/Slippy_map_tilenames
   */
  def tileLocation(tile: Tile): Location = {
    Location(
      math.atan(
        math.sinh(
          math.Pi - ((tile.y.toDouble / math.pow(2.0, tile.zoom.toDouble)) * 2.0 * math.Pi)))
        * (180.0 / math.Pi),
      (tile.x.toDouble / math.pow(2.0, tile.zoom.toDouble)) * 360.0 - 180.0)
  }

  /**
   * @param temperatures Known temperatures
   * @param colors Color scale
   * @param tile Tile coordinates
   * @return A 256×256 image showing the contents of the given tile
   */
  def tile(temperatures: Iterable[(Location, Temperature)], colors: Iterable[(Temperature, Color)], tile: Tile, size: Int): Image = {
    val tileScale = (size * tile.x, size * tile.y, tile.zoom + (math.log(size) / math.log(2)).toInt)
    val coordIter = for (y <- (0 until size); x <- (0 until size)) yield (x,y)
    val colorArr = coordIter.par.map{case (x,y) => Visualization.interpolateColor(
      colors,
      Visualization.predictTemperature(temperatures, tileLocation(Tile(tileScale._1 + x, tileScale._2 + y, tileScale._3))))}

    Image(size, size, colorArr.map(color => Pixel(color.red, color.green, color.blue, alpha)).toArray)
  }
  
  def tile(temperatures: Iterable[(Location, Temperature)], colors: Iterable[(Temperature, Color)], tile: Tile): Image = {
    this.tile(temperatures, colors, tile, tilesize)
  }
  
  def tileToImg(colors: Iterable[(Temperature, Color)], tileSize: Int, scale: Int)(year: Int, tile: Tile, temperatures: Iterable[(Location, Temperature)]): Unit = {
    val img = this.tile(temperatures, colors, tile, tileSize).scaleTo(scale, scale)
    new java.io.File(s"target/temperatures/$year/${tile.zoom}").mkdirs()
    img.output(new java.io.File(s"target/temperatures/$year/${tile.zoom}/${tile.x}-${tile.y}.png"))
    ()
  }

  /**
   * Generates all the tiles for zoom levels 0 to 3 (included), for all the given years.
   * @param yearlyData Sequence of (year, data), where `data` is some data associated with
   *                   `year`. The type of `data` can be anything.
   * @param generateImage Function that generates an image given a year, a zoom level, the x and
   *                      y coordinates of the tile and the data to build the image from
   */
  def generateTiles[Data](
    yearlyData:    Iterable[(Year, Data)],
    generateImage: (Year, Tile, Data) => Unit): Unit = {
    generateTiles(yearlyData,generateImage,(0 to 3))
  }

  def generateTiles[Data](
    yearlyData:    Iterable[(Year, Data)],
    generateImage: (Year, Tile, Data) => Unit,
    zoomRange: Range.Inclusive): Unit = {
    yearlyData.foreach {
      case (year, data) =>
        val scaleRng = for (scale <- zoomRange; x <- (0 until 1 << scale); y <- (0 until 1 << scale)) yield (scale, x, y)
        scaleRng.par.foreach { case (scale, x, y) => generateImage(year, Tile(x, y, scale), data)}
//        for (scale <- (zoomRange)) {
//          val scalePow = math.pow(2.0, scale.toDouble).toInt
//          val combinations = for (x <- (0 until scalePow); y <- (0 until scalePow)) yield (x, y)
//          combinations.par.foreach { case (x, y) => generateImage(year, Tile(x, y, scale), data) }
//        }
    }
  }
  
}
