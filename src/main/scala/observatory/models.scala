package observatory

/**
 * Introduced in Week 1. Represents a location on the globe.
 * @param lat Degrees of latitude, -90 ≤ lat ≤ 90
 * @param lon Degrees of longitude, -180 ≤ lon ≤ 180
 */
case class Location(lat: Double, lon: Double) {
  require(-90.0 <= lat && lat <= 90.0)
  require(-180.0 <= lon && lon <= 180.0)
  
  final val equatorialRadius = 6378.137 //in km
  final val poleRadius = 6356.752 //in km
  final val meanEarthRadius = 6371.0087 // 1.0 / 3.0 * ((2.0 * equatorialRadius) + poleRadius)
  final val meanEarthRadiusAntipode = 20015.114 // meanEarthRadius * math.Pi

  def centralAngle(to: Location): Double =
    math.acos(math.sin(lat.toRadians) * math.sin(to.lat.toRadians) + math.cos(lat.toRadians) * math.cos(to.lat.toRadians) * math.cos(math.abs(lon.toRadians - to.lon.toRadians)))

  def checkAntipodes(x: Location): Boolean =
    lat == -1 * x.lat && (lon == x.lon + 180 || lon == x.lon - 180)

  def greatCircleDistance(to: Location): Double = {
    if (lat == to.lat && lon == to.lon) 0
    else if (checkAntipodes(to)) meanEarthRadiusAntipode
    else meanEarthRadius * centralAngle(to)
  }
  
  def toGridLocation() : GridLocation = 
    GridLocation(
        math.min(-89,math.round(lat).toInt),
        math.max(math.round(lon).toInt,179)
        )
  def toCellPoint(): CellPoint = 
    CellPoint(math.abs(lat - lat.floor.toInt),math.abs(lon - lon.floor.toInt))
}

/**
 * Introduced in Week 3. Represents a tiled web map tile.
 * See https://en.wikipedia.org/wiki/Tiled_web_map
 * Based on http://wiki.openstreetmap.org/wiki/Slippy_map_tilenames
 * @param x X coordinate of the tile
 * @param y Y coordinate of the tile
 * @param zoom Zoom level, 0 ≤ zoom ≤ 19
 */
case class Tile(x: Int, y: Int, zoom: Int) {
  require(0 <= zoom && zoom <= 19)
}

/**
 * Introduced in Week 4. Represents a point on a grid composed of
 * circles of latitudes and lines of longitude.
 * @param lat Circle of latitude in degrees, -89 ≤ lat ≤ 90
 * @param lon Line of longitude in degrees, -180 ≤ lon ≤ 179
 */
case class GridLocation(lat: Int, lon: Int) {
  require(-89 <= lat && lat <= 90, "lat " + lat + " has to be in range [-89,90]")
  require(-180 <= lon && lon <= 179)
  def toLocation(): Location = Location(lat.toDouble,lon.toDouble)
  def increaseLat(): GridLocation = GridLocation(math.min(90,lat + 1),lon)
  def increaseLon(): GridLocation = GridLocation(lat,math.min(lon + 1,179))
  def decreaseLat(): GridLocation = GridLocation(math.max(-89,lat - 1),lon)
  def decreaseLon(): GridLocation = GridLocation(lat,math.max(lon - 1,-180))
}

/**
 * Introduced in Week 5. Represents a point inside of a grid cell.
 * @param x X coordinate inside the cell, 0 ≤ x ≤ 1
 * @param y Y coordinate inside the cell, 0 ≤ y ≤ 1
 */
case class CellPoint(x: Double, y: Double) {
  require(0.0 <= x && x <= 1.0)
  require(0.0 <= y && y <= 1.0)
}

/**
 * Introduced in Week 2. Represents an RGB color.
 * @param red Level of red, 0 ≤ red ≤ 255
 * @param green Level of green, 0 ≤ green ≤ 255
 * @param blue Level of blue, 0 ≤ blue ≤ 255
 */
case class Color(red: Int, green: Int, blue: Int) {
  private def requireRGBInterval(color: Int*): Boolean = color.map(x => 0 <= x && x <= 255).reduce(_ && _)
  requireRGBInterval(red,green,blue)
}

